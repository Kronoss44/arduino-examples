#include <Timer.h>
#include <LiquidCrystal.h>
#define PIN50             1
#define PIN100            0

#define PINLED_100PERCENT A2
#define PINLED_50PERCENT  A1
#define PINLED_INFERIOR   A0

#define PINRS             12
#define PIN_ENABLE        11
#define PIND4             5
#define PIND5             4
#define PIND6             3
#define PIND7             2

#define TANK_LITROS       20//Litros

#define DEBUG
enum
{
  SECONDS,
  MINUTES,
  HOURS
};
enum
{
  STATE_INFERIOR,
  STATE_50_PERCENT,
  STATE_100_PERCENT
};
byte TANK_STATE=-1;
Timer t, timmerlcd,timerkill;
LiquidCrystal lcd(PINRS, PIN_ENABLE, PIND4, PIND5, PIND6, PIND7);
void setup()
{
  lcd.begin(16, 2);
  lcd.print("Medidor de agua cargado correctamente");
  lcd.display();
  timmerlcd.every(100, timmer_lcd, 22);
  timerkill.every(2200, timmer_kill, 1);
  Serial.begin(9600);
  t.every(SSetTime(SECONDS, 5), timmer_finish);
  Serial.println("TANK STATE: INIT");
  pinMode(PIN50, INPUT_PULLUP);
  pinMode(PIN100, INPUT_PULLUP);
  analogWrite(PINLED_INFERIOR, 255);
  analogWrite(PINLED_50PERCENT, 255);
  analogWrite(PINLED_100PERCENT, 255);
}

void loop() 
{
  t.update();
  timmerlcd.update();
  timerkill.update();
  if(TANK_STATE == STATE_INFERIOR)
  {
      #if defined DEBUG
      Serial.println("TANK STATE: INFERIOR");
      #endif
  }
  else if(TANK_STATE == STATE_50_PERCENT)
  {
      #if defined DEBUG
      Serial.println("TANK STATE: 50 PERCENT");
      #endif
  }
  else if(TANK_STATE == STATE_100_PERCENT)
  {
      #if defined DEBUG
      Serial.println("TANK STATE: 100 PERCENT");
      #endif
  }
}
void timmer_kill()
{
  lcd.clear();
}
void timmer_lcd()
{
  for (int positionCounter = 0; positionCounter < 2; positionCounter++)
  {
    lcd.scrollDisplayRight();
  }
}
void timmer_finish()
{
  int sensorval[2];
  sensorval[0] = digitalRead(PIN50);
  sensorval[1] = digitalRead(PIN100);
  
  analogWrite(PINLED_INFERIOR, 0);
  analogWrite(PINLED_50PERCENT, 0);
  analogWrite(PINLED_100PERCENT, 0);
  
  if(sensorval[0] == HIGH && sensorval[1] == HIGH)
  {
    TANK_STATE = STATE_INFERIOR;
    analogWrite(PINLED_INFERIOR, 255);
    analogWrite(PINLED_50PERCENT, 0);
    analogWrite(PINLED_100PERCENT, 0);
    
    String percent;
    lcd.clear();
    lcd.home();
    lcd.print("Tanque:");
    lcd.setCursor(0, 1);
    percent = String("Menor que ");
    percent = percent + TANK_LITROS/2;
    percent = percent + "L";
    lcd.print(percent);
  }
  else if(sensorval[1] == HIGH && sensorval[0] == LOW)
  {
    TANK_STATE = STATE_50_PERCENT;
    analogWrite(PINLED_INFERIOR, 0);
    analogWrite(PINLED_50PERCENT, 255);
    analogWrite(PINLED_100PERCENT, 0);

    String percent;
    lcd.clear();
    lcd.home();
    lcd.print("Tanque:");
    lcd.setCursor(0, 1);
    percent = String("Almenos ");
    percent = percent + TANK_LITROS/2;
    percent = percent + "L";
    lcd.print(percent);
  }
  else if(sensorval[0] == LOW && sensorval[1] == LOW)
  {
    TANK_STATE = STATE_100_PERCENT;
    analogWrite(PINLED_INFERIOR, 0);
    analogWrite(PINLED_50PERCENT, 0);
    analogWrite(PINLED_100PERCENT, 255);

    String percent;
    lcd.clear();
    lcd.home();
    lcd.print("Tanque:");
    lcd.setCursor(0, 1);
    percent = percent + TANK_LITROS;
    percent = percent + "L";
    lcd.print(percent);
  }
}
int SSetTime(int type, int value)
{
  int result;
  switch(type)
  {
    case SECONDS:
    {
      result = 1000 * value;
      return result;
    }
    case MINUTES:
    {
      result = 1000 * value *60;
      return result;
    }
    case HOURS:
    {
      result = 1000 * value *60 *60;
      return result;
    }
    default:
    {
      return 0;
    }
  }
}
