//pin config.
int pinSpeaker = 5;
int pinButton = 6;
int pinOut = 7;
int pinSegOff = 8;
int pinSegOn = 9;

void setup() 
{
  //why here? is for more optimization of code
  Serial.begin(9600);
  pinMode(pinSegOn, OUTPUT);
  pinMode(pinSegOff, OUTPUT);
  pinMode(pinOut, OUTPUT);
  pinMode(pinButton, INPUT);
}

void loop() 
{
    int buttonState = digitalRead(pinButton);
    if(buttonState == HIGH)
    {
        digitalWrite(pinOut, HIGH);
        digitalWrite(pinSegOn, HIGH);
        digitalWrite(pinSegOff, LOW);
        tone(pinSpeaker, 420, 1000);
    }
    else
    {
        digitalWrite(pinOut, LOW);
        digitalWrite(pinSegOn, LOW);
        digitalWrite(pinSegOff, HIGH);
        tone(pinSpeaker, 420, 1000);//play 420 tone on pin 5 for 1s
    }
}
