#ES
## Repositorio de ejemplos de arduino
*Pequeños ejemplos de sketch's en arduino, comentados con sus respectivas piezas necesarias.*
*También se incluye un archivo* **.simu** *para poder observar la simulación en tiempo real del sketch*
## Dependencias
**Para poder probar los ejemplos sólo basta de los siguientes programas**
- [SimuIDE](https://simulide.blogspot.com/)
- [Arduino](https://www.arduino.cc/)
- [Git](https://git-scm.com/download/win) **Opcional para clonar este repositorio**
## Documentación
*En cada ejemplo viene adjuntado su uso*